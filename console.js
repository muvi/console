/////////////////////////////////////////////////////////////////////////////////////////////
//
// console
//
//    Module that shows a console that can be used to load packages with pix.
//
// License
//    Apache License Version 2.0
//
// Copyright Nick Verlinden
//
///////////////////////////////////////////////////////////////////////////////////////////// 
/////////////////////////////////////////////////////////////////////////////////////////////
//
// Dependencies
//
/////////////////////////////////////////////////////////////////////////////////////////////
var cli =    require("cli");
var host =   require("host");
var type =   require("type");
var string = require("string-validation");

/////////////////////////////////////////////////////////////////////////////////////////////
//
// Constants
//
/////////////////////////////////////////////////////////////////////////////////////////////
var MEMORY_LIMIT = 128;

/////////////////////////////////////////////////////////////////////////////////////////////
//
// Module definition
//
/////////////////////////////////////////////////////////////////////////////////////////////
define(["pkx", "configuration"], function app(pkx, configuration) {
    var exit = false;
    var readline; 
    var appDiv, backgroundDiv, foregroundDiv, logDiv, promptDiv, input, promptSpan, promptDiv;
    var options = {
        "repo" : "https://gitlab.com/pix-console/", 
        "textWrap" : false,
        "logErrors" : false,
        "logInstance" : false
    };
    var memory = [];
    var memoryIdx = 0;

    if (host.isRuntimeBrowserFamily()) {
        options.logErrors = true;

        appDiv = document.createElement("div");
        appDiv.className = "console-app-div";

        backgroundDiv = document.createElement("div");
        backgroundDiv.className = "console-background-div";

        foregroundDiv = document.createElement("div");
        foregroundDiv.className = "console-foreground-div";

        logDiv = document.createElement("div");
        logDiv.className = "console-log-div";

        promptDiv = document.createElement("footer");
        promptDiv.className = "console-prompt-div";
        
        input = document.createElement("textarea");
        input.className = "console-prompt-input";
        input.rows = 1;
        input.addEventListener("keyup", textAreaKeyUp);
        input.addEventListener("keydown", textAreaKeyDown);

        promptSpan = document.createElement("span");
        promptSpan.className = "console-prompt-span";
        promptSpan.innerHTML = ">&nbsp;";

        promptDiv.appendChild(promptSpan);
        promptDiv.appendChild(input);
        foregroundDiv.appendChild(logDiv);
        foregroundDiv.appendChild(promptDiv);
        appDiv.appendChild(backgroundDiv);
        appDiv.appendChild(foregroundDiv);
        document.body.appendChild(appDiv);

        var originalLog = console.log;
        var originalError = console.error;
        var originalWarn = console.warn;
        var originalDebug = console.debug;

        console.log = function(txt) {
            addLog(txt.toString(), "console-log-info");
            originalLog.apply(console, arguments);
        }

        console.error = function(txt) {
            addLog(txt.toString(), "console-log-error");
            originalError.apply(console, arguments);
        }

        console.warn = function(txt) {
            addLog(txt.toString(), "console-log-warning");
            originalWarn.apply(console, arguments);
        }

        console.debug = function(txt) {
            addLog(txt.toString(), "console-log-debug");
            originalDebug.apply(console, arguments);
        }
    }
    else if (host.isRuntimeNodeFamily()) {
        try {
            var rl = require("readline");
            readline = rl.createInterface({
                input:  process.stdin,
                output: process.stdout
            });

            promptNode();
        }
        catch(e) {
            console.error(e);
        }
    }
    else {
        console.error("Unsupported platform.");
    }

    function addLog(txt, className, skipResize) {
        if (!className) {
            className = "";
        }
        var follow = false;

        if (!skipResize) {
            var lastChild = logDiv.childNodes[logDiv.childNodes.length - 1];
            if (logDiv.scrollTop + logDiv.offsetHeight >= logDiv.scrollHeight - (lastChild? lastChild.offsetHeight : 0)) {
                follow = true;
            }
        }

        var span = document.createElement("span");
        span.className = "console-log-span " + className;
        span.innerText = txt;
        logDiv.appendChild(span);

        if (follow) {
            logDiv.scrollTop = logDiv.scrollHeight;
        }
    }

    function textAreaKeyUp(e) {
        var code = e.keyCode ? e.keyCode : e.which;
        if (code == 13 && !e.shiftKey) {
            e.preventDefault();

            promptBrowser();
        }

        // reset textaerea height
        input.style.height = "1px";
        input.style.height = (input.scrollHeight)+"px";

        if (code == 13) {
            return false;
        }
    }

    function textAreaKeyDown(e) {
        var code = e.keyCode ? e.keyCode : e.which;
        if (code == 40) {
            // up key
            memoryIdx++;
            if (memoryIdx > memory.length) {
                memoryIdx = memory.length;
            }
        }
        else if (code == 38) {
            // down key
            memoryIdx--;
            if (memoryIdx < 0) {
                memoryIdx = 0;
            }
        }

        if (code == 38 || code == 40) {
            input.value = memory[memoryIdx] || "";

            e.preventDefault();
            return false;
        }
    }

    function processingDone(e) {
        if (options.logErrors && e instanceof Error) {
            console.error(e);
        }

        if(options.logInstance && e && e.instance) {
            console.debug(type.isObject(e.instance)? JSON.stringify(e.instance, null, "    ") : type.getType(e.instance));
        }
    }

    function promptBrowser(e) {
        var line = input.value.trim();
        var follow = false;

        var lastChild = logDiv.childNodes[logDiv.childNodes.length - 1];
        if (logDiv.scrollTop + logDiv.offsetHeight >= logDiv.scrollHeight - (lastChild? lastChild.offsetHeight : 0)) {
            follow = true;
        }

        addLog("> " + line, "", true);
        input.value = "";

        if (follow) {
            logDiv.scrollTop = logDiv.scrollHeight;
        }

        if (line && line != "") {
            memory.push(line);
            if (memory.length > MEMORY_LIMIT) {
                memory.splice(0, 1);
            }
        }

        memoryIdx = memory.length;

        processLine(line, processingDone);
    }

    function promptNode(e) {
        processingDone(e);

        readline.question("> ", function(line) {
            line = line.trim();

            switch(line) {
                case "exit":
                    readline.close();
                    break;
                default:
                    processLine(line, promptNode);
            }
        });
    }

    function processLine(line, callback) {
        if (!line || line == "") {
            return;
        }

        switch(line) {
            case "@text-wrap":
                if (!host.isRuntimeBrowserFamily()) {
                    console.error("This option is only available in a browser runtime.");
                    break;
                }
                options.textWrap = !options.textWrap;
                console.log("text-wrap: " + (options.textWrap? "on" : "off"));
                logDiv.setAttribute("data-wrap", (options.textWrap? "enable" : ""));
                callback();
                break;
            case "@log-errors":
                options.logErrors = !options.logErrors;
                console.log("log-errors: " + (options.logErrors? "on" : "off"));
                callback();
                break;
            case "@log-instance":
                options.logInstance = !options.logInstance;
                console.log("log-instance: " + (options.logInstance? "on" : "off"));
                callback();
                break;
            default:
                exec(cli.split(line), callback? callback : null);
        }
    }

    function exec(argv, callback) {
        pix({ "selector" : (string.isURL(argv[0])? argv[0] : options.repo + argv[0]), "config" : { "argv" : argv }}).then(callback).catch(callback);
    }
});